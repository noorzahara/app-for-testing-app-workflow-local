# About

This repository contains an IGB App for testing App installation and update workflows.

Check the Downloads folder for different versions. 

To determine which IGB version an App version works with, review its MANIFEST.MF or review commits.
